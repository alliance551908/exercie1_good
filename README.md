| |
|---|
| **Flask Docker Pytest GitLab CI/CD Example** |
| Ce projet est une démonstration de l'utilisation de Docker, Pytest et GitLab CI/CD pour une application Flask simple. Il comprend la containerisation de l'application, l'écriture de tests unitaires et la configuration d'un pipeline CI/CD. |
| **Structure du Projet** |
| . <br> ├── app.py <br> ├── requirements.txt <br> ├── Dockerfile <br> ├── tests <br> │ └── test_app.py <br> └── .gitlab-ci.yml |
| **Prérequis** |
| Docker <br> Python 3.8+ <br> Git |
| **Étapes** |
| **1. Application Flask** <br> L'application Flask simple se trouve dans app.py : <br> from flask import Flask <br> app = Flask(__name__) <br> <br> @app.route('/') <br> def home(): <br>     return "Bonjour, le monde!" <br> <br> if __name__ == "__main__": <br>     app.run(host='0.0.0.0', port=5000) |
| **2. Dépendances** <br> Les dépendances sont listées dans le fichier requirements.txt : <br> Flask <br> pytest |
| **3. Dockerisation** <br> Le fichier Dockerfile pour containeriser l'application Flask est le suivant : <br> FROM python:3.8-slim <br> WORKDIR /app <br> COPY requirements.txt requirements.txt <br> RUN pip install -r requirements.txt <br> COPY . . <br> CMD ["python", "app.py"] |
| **4. Tests Unitaires avec Pytest** <br> Les tests unitaires sont écrits dans le fichier tests/test_app.py : <br> import pytest <br> from app import app <br> <br> @pytest.fixture <br> def client(): <br>     app.config['TESTING'] = True <br>     with app.test_client() as client: <br>         yield client <br> <br> def test_home(client): <br>     rv = client.get('/') <br>     assert rv.data == b'Bonjour, le monde!' |
| **5. Pipeline GitLab CI/CD** <br> Le pipeline GitLab CI/CD est défini dans le fichier .gitlab-ci.yml : <br> stages: <br>   - build <br>   - test <br>   - deploy <br> <br> build_image: <br>   stage: build <br>   image: docker:stable <br>   services: <br>     - docker:dind <br>   before_script: <br>     - docker info <br>   script: <br>     - docker build -t my-flask-app . <br>   artifacts: <br>     paths: <br>       - app.py <br>       - Dockerfile <br>       - requirements.txt <br>       - tests/ <br>   only: <br>     - main <br> <br> test_app: <br>   stage: test <br>   dependencies: <br>     - build_image <br>   image: python:3.8 <br>   before_script: <br>     - pip install -r requirements.txt <br>   script: <br>     - pytest tests/ <br>   only: <br>     - main <br> <br> deploy: <br>   stage: deploy <br>   image: alpine:latest <br>   script: <br>     - echo "Déploiement sur le serveur de production..." <br>   only: <br>     - main |
| **Instructions pour l'Utilisation** |
| Clonez le dépôt : <br> git clone <url_du_dépôt> <br> cd <nom_du_dépôt> <br> Construisez l'image Docker : <br> docker build -t my-flask-app . <br> Exécutez l'application Flask dans un conteneur Docker : <br> docker run -p 5000:5000 my-flask-app <br> Exécutez les tests unitaires avec Pytest : <br> pytest tests/ <br> Poussez vos modifications sur le dépôt GitLab pour déclencher le pipeline CI/CD. |
| **Contribuer** |
| Les contributions sont les bienvenues ! Veuillez soumettre une issue ou un pull request pour discuter des modifications que vous souhaitez apporter. |
