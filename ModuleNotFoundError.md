# Rappel de l'exercice :

Cette évaluation est conçue pour tester nos connaissances et compétences dans l'utilisation de Docker, Pytest et GitLab CI/CD. Vous devrez accomplir les tâches suivantes :

1. Dockeriser une Application Python Simple
2. Écrire des Tests Unitaires avec Pytest
3. Créer un Pipeline GitLab CI pour Construire, Tester et Déployer l'Application

## Structure du Projet

| Chemin                | Contenu                                                                                          |
|-----------------------|--------------------------------------------------------------------------------------------------|
| `app.py`              | Application Flask principale                                                                     |
| `requirements.txt`    | Dépendances du projet                                                                            |
| `Dockerfile`          | Instructions pour dockeriser l'application                                                       |
| `tests/test_app.py`   | Tests unitaires pour l'application                                                               |
| `tests/conftest.py`   | Configuration Pytest pour ajuster le chemin Python                                               |
| `.gitlab-ci.yml`      | Pipeline CI/CD pour construire, tester et déployer l'application                                 |

## Modification nécessaire

| Fichier Modifié         | Description                                                                                             | Raison de la Modification                                                                                                                                                                                                                                                                               |
|-------------------------|---------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `tests/conftest.py`     | Le fichier `conftest.py` est ajouté pour configurer le PYTHONPATH afin que le module `app` soit trouvé lors des tests avec Pytest.                                   | Lorsque Pytest exécute les tests, le répertoire courant n'est pas inclus dans le PYTHONPATH par défaut. En ajustant le chemin avec `os.path.abspath`, nous permettons à Pytest d'importer correctement le module `app`, assurant ainsi le bon fonctionnement des tests.                                      |
| `.gitlab-ci.yml`        | La ligne `export PYTHONPATH=$PYTHONPATH:.` est ajoutée dans `before_script` pour inclure le répertoire courant dans le PYTHONPATH lors des tests dans le pipeline GitLab CI. | Dans l'environnement CI/CD de GitLab, les variables d'environnement ne sont pas configurées pour inclure le répertoire du projet dans le PYTHONPATH par défaut. En ajoutant cette commande, nous assurons que le module `app` peut être trouvé lors des tests Pytest, assurant ainsi une intégration continue sans erreur. |
